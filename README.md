
# PRUEBA-KRUGER

El proyecto **PRUEBA-KRUGER** usando como ayuda los proyectos `spring-boot, spring-data y swagger`.

# Requisitos

* java version 14
* maven 3.6.3

# Parametros de configuración

***aplication.properties***

* spring.application.name=xxxxxxxx
* server.port=XXXX
* server.servlet.context-path=/xxxxx/xxxx/xxxx

# Como levantar el proyecto

Para ello necesitamos saber los siguientes puntos:

- Directorio donde se encuentra el proyecto ejemplo: home/testing/Descargas/
- Nombre del proyecto
- Path donde se encuentra el archivo de conf `application.properties`.

# Ruta del Swagger

***URL***

```
http://localhost:5001/apis/kruger/swagger-ui.html#/
```



package ec.prueba.kruger.util;

public enum StatusUtil {
	
	CREATED(100, "Created"),
	NOT_CREATED(101, "Not Created"),
	UPDATED(102, "Update"),
	NOT_UPDATED(103, "Not Update"),
	FOUND(104, "Found"),
	NOT_FOUND(105, "Not Found"),
	DELETED(106, "Deleted"),
	NOT_DELETED(107, "Not Deleted"),
	FAILED(108, "Failed"),
	GENERATED(109, "Generated"),
	NONE(110, "None"),
	OK(200,"Ok"),
	UNAUTHORIZED(401,"Unauthorized"),
	PROCESSING(402,"Processing"),
	INTERNAL_SERVER_ERROR(500,"Internal Server Error");

	private final Integer value;
	
	private final String message;

	private StatusUtil(Integer value, String message) {
		this.value = value;
		this.message = message;
	}

	public Integer getValue() {
		return value;
	}

	public String getMessage() {
		return message;
	}

}

package ec.prueba.kruger.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import ec.prueba.kruger.dto.CreateResponse;
import ec.prueba.kruger.dto.EmployeeRegister;
import ec.prueba.kruger.dto.EmployeeUpdate;
import ec.prueba.kruger.dto.Response;

public interface EmployeeApi {

	@PostMapping("/create")
	@CrossOrigin(origins = "*")

	public ResponseEntity<CreateResponse> create(HttpServletRequest header, @RequestHeader String channel,
			@RequestHeader String app, @RequestHeader String user, @Validated @RequestBody EmployeeRegister register);

	@PutMapping("/update{identification}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Response> updateEmployee(HttpServletRequest header, @RequestHeader String channel,
			@RequestHeader String app, @RequestHeader String user, @PathVariable String identification,
			@Validated @RequestBody EmployeeUpdate update);

	@DeleteMapping("/deleted{identification}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Response> deletedByIdentification(HttpServletRequest header, @RequestHeader String channel,
			@RequestHeader String app, @RequestHeader String user, @PathVariable String identification);

	@DeleteMapping("/deleted{email}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Response> deletedByEmail(HttpServletRequest header, @RequestHeader String channel,
			@RequestHeader String app, @RequestHeader String user, @PathVariable String email);

	@GetMapping("/find/email")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Response> findByEmail(HttpServletRequest header, @RequestHeader String channel,
			@RequestHeader String app, @RequestHeader String user, @RequestParam String email);

	@GetMapping("/find/identification")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Response> findByIdentification(HttpServletRequest header, @RequestHeader String channel,
			@RequestHeader String app, @RequestHeader String user, @RequestParam String identification);

	@GetMapping("/find")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Response> find(HttpServletRequest header, @RequestHeader String channel, @RequestHeader String app, @RequestHeader String user,
			@RequestParam(defaultValue = "false", required = false) Boolean vaccinated, @RequestParam(defaultValue = "", required = false) String vaccineType, @RequestParam Date startDate, @RequestParam Date endDate);

}

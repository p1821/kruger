package ec.prueba.kruger.controller;

import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.prueba.kruger.dto.CreateResponse;
import ec.prueba.kruger.dto.EmployeeRegister;
import ec.prueba.kruger.dto.EmployeeUpdate;
import ec.prueba.kruger.dto.Header;
import ec.prueba.kruger.dto.Response;
import ec.prueba.kruger.service.EmployeeServiceImpl;

@RestController
@RequestMapping("/employee")
public class EmployeeController implements EmployeeApi{

		private static final String ID = "id";
		private String logID;
		
		@Autowired
		EmployeeServiceImpl employeeServiceImpl;
		
		@Override
		public ResponseEntity<CreateResponse> create(HttpServletRequest header, String channel, String app, String user, EmployeeRegister register){
			logID=UUID.randomUUID().toString();
			ThreadContext.put(ID, logID);
			
			Header headers = new Header(header);
				
			return new ResponseEntity<>(employeeServiceImpl.create(headers, register),HttpStatus.OK);
		}
		
		@Override
		public ResponseEntity<Response> updateEmployee(HttpServletRequest header, String channel, String app, String user, String identification, EmployeeUpdate update){
			logID=UUID.randomUUID().toString();
			ThreadContext.put(ID, logID);
			
			Header headers = new Header(header);
				
			return new ResponseEntity<>(employeeServiceImpl.updateEmployee(headers, identification, update),HttpStatus.OK);
		}
		
		@Override
		public ResponseEntity<Response> deletedByIdentification(HttpServletRequest header, String channel, String app, String user, String identification){
			logID=UUID.randomUUID().toString();
			ThreadContext.put(ID, logID);
			
			Header headers = new Header(header);
				
			return new ResponseEntity<>(employeeServiceImpl.deletedByIdentification(headers, identification),HttpStatus.OK);
		}
		
		@Override
		public ResponseEntity<Response> deletedByEmail(HttpServletRequest header, String channel, String app, String user, String email){
			logID=UUID.randomUUID().toString();
			ThreadContext.put(ID, logID);
			
			Header headers = new Header(header);
				
			return new ResponseEntity<>(employeeServiceImpl.deletedByEmail(headers, email),HttpStatus.OK);
		}
		@Override
		public ResponseEntity<Response> findByEmail(HttpServletRequest header, String channel, String app, String user, String email){
			logID=UUID.randomUUID().toString();
			ThreadContext.put(ID, logID);
			
			Header headers = new Header(header);
				
			return new ResponseEntity<>(employeeServiceImpl.findByEmail(headers, email),HttpStatus.OK);
		}
		@Override
		public ResponseEntity<Response> findByIdentification(HttpServletRequest header, String channel, String app, String user, String identification){
			logID=UUID.randomUUID().toString();
			ThreadContext.put(ID, logID);
			
			Header headers = new Header(header);
				
			return new ResponseEntity<>(employeeServiceImpl.findByIdentification(headers, identification),HttpStatus.OK);
		}
		@Override
		public ResponseEntity<Response> find(HttpServletRequest header, String channel, String app, String user, Boolean vaccinated, String vaccineType, Date startDate, Date endDate){
			logID=UUID.randomUUID().toString();
			ThreadContext.put(ID, logID);
			
			Header headers = new Header(header);
				
			return new ResponseEntity<>(employeeServiceImpl.find(headers, vaccinated, vaccineType, startDate, endDate),HttpStatus.OK);
		}
}

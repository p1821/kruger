package ec.prueba.kruger.dto;

import ec.prueba.kruger.util.StatusUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)

public class CreateResponse extends Response{
	
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	
	public CreateResponse(Response response) {
		super(response);
	}

	public CreateResponse(StatusUtil result) {
		super(result);
	}
	
}

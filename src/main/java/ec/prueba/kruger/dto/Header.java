package ec.prueba.kruger.dto;

import java.io.Serializable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Header implements Serializable{

	private static final long serialVersionUID = 1L;
	private String channel;
	private String deviceId;
	private String app;
	private String user;
	
	public Header(HttpServletRequest request) {
		super();
		this.channel = request.getHeader("channel");
		this.deviceId = request.getRemoteHost();
		this.app = request.getHeader("app");
		this.user = request.getHeader("user");
	}

	public Header(HttpServletRequest request,String channel, String app, String user) {
		super();
		this.deviceId = request.getRemoteHost();
		this.channel = channel;
		this.app = app;
		this.user = user;
	}

	public Map<String, String> getHeaderMap() {
		return (Map.of("app", this.app, "channel", this.channel, "deviceId", this.deviceId, "user", this.user));
	}

}

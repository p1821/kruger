package ec.prueba.kruger.dto;

import java.io.Serializable;
import lombok.*;


@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeRegister implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@NonNull
	private String identification;
	@NonNull
	private String firstName;
	@NonNull
	private String lastName;
	@NonNull
	private String email;
}

package ec.prueba.kruger.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class EmployeeUpdate implements Serializable{

	private static final long serialVersionUID = 1L;

	private Date dateOfBirth;
	private String address;
	private String telephone;
	private Boolean vaccinated;
	private String vaccineType;
	private Date dateVaccine;
	private Integer numberDoses;

}

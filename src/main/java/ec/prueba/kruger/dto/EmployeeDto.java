package ec.prueba.kruger.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.*;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor

public class EmployeeDto extends EmployeeRegister implements Serializable{

	private static final long serialVersionUID = 1L;

	private Date dateOfBirth;
	private String address;
	private String telephone;
	private Boolean vaccinated;
	private String vaccineType;
	private Date dateVaccine;
	private Integer numberDoses;

}

package ec.prueba.kruger.dto;

import ec.prueba.kruger.util.StatusUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ResponseEmployee extends Response{

	private static final long serialVersionUID = 1L;
	private EmployeeDto employeeData;
	
	public ResponseEmployee(Response response) {
		super(response);
	}

	public ResponseEmployee(StatusUtil result) {
		super(result);
	}

	public ResponseEmployee(Response response, EmployeeDto employeeData) {
		super(response);
		this.employeeData = employeeData;
	}
}

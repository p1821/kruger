package ec.prueba.kruger.dto;

import java.io.Serializable;

import ec.prueba.kruger.util.StatusUtil;
import lombok.Data;

@Data
public class Response implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer code;
	private String message;
	private String description;
	
	
	public Response(Response response) {
		super();
		this.code = response.getCode();
		this.message = response.getMessage();
		this.description = response.getDescription();
	}
	
	public Response(StatusUtil result) {
		super();
		this.code = result.getValue();
		this.message = result.getMessage();
	}	
}

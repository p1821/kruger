package ec.prueba.kruger.dto;

import java.util.List;

import ec.prueba.kruger.util.StatusUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ResponseAll extends Response{

	private static final long serialVersionUID = 1L;
	private List<EmployeeDto> EmployeeData;
	
	public ResponseAll(Response response) {
		super(response);
	}

	public ResponseAll(StatusUtil result) {
		super(result);
	}

	public ResponseAll(Response response, List<EmployeeDto> employeeData) {
		super(response);
		this.EmployeeData = employeeData;
	}

	
}

package ec.prueba.kruger.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.prueba.kruger.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	
	public List<Employee> findByIdentificationAndStatus(String identification, String status);
	public List<Employee> findByEmailIgnoreCaseAndStatus(String email, String status);
	public List<Employee> findByVaccinatedAndStatus(Boolean vaccinated, String status);
	public List<Employee> findByVaccineTypeAndStatus(String vaccineType, String status);
	public List<Employee> findByVaccinatedAndVaccineTypeAndDateVaccineBetweenAndStatus(Boolean vaccinated,String vaccineType, Date startDate, Date endDate, String status);

}

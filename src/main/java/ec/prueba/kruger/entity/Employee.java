package ec.prueba.kruger.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Table(name= Employee.TABLE)

@Entity
public class Employee implements Serializable{
	
	protected static final String TABLE = "EMPLEADOS";
	protected static final String SEQ ="EMPLEADO_SEQ";
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ)
	@SequenceGenerator(name=SEQ, sequenceName = SEQ,  allocationSize = 1)
	
	@Column(name = "ID_EMPLEADO")
	private long idEmployee;
	
	@Column(name = "CEDULA", unique = true, precision = 10)
	private String identification;

	@Column(name = "NOMBRE", unique = false, length = 100)
	private String firstName;
	
	@Column(name = "APELLIDO", unique = false, length = 100)
	private String lastName;
	
	@Column(name = "EMAIL", unique = false, length = 100)
	private String email;
	
	@Column(name = "USUARIO", unique = false, length = 100)
	private String userName;
	
	@Column(name = "CLAVE", unique = false, length = 100)
	private String password;
	
	@Column(name = "FECHA_NACIMIENTO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateOfBirth;
	
	@Column(name = "DIRECCION", unique = false, length = 2000)
	private String address;
	
	@Column(name = "TELEFONO", unique = false, length = 15)
	private String telephone;
	
	@Column(name = "VACUNADO", unique = false)
	private Boolean vaccinated;
	
	@Column(name = "TIPO_VACUNA", unique = false, length = 100)
	private String vaccineType;
	
	@Column(name = "FECHA_VACUNA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVaccine;
	
	@Column(name = "DOSIS", unique = false)
	private Integer numberDoses;
	
	@Column(name = "ROL", unique = false, length = 100)
	private String role;
	
	@Column(name = "ESTADO", unique = false, length = 100)
	private String status;
	
	@Column(name = "USUARIO_CREADOR", unique = false, length = 100)
	private String userCreate;
	
	@Column(name = "USUARIO_MODIFICACION", unique = false, length = 100)
	private String userModify;
	
	@Column(name = "FECHA_CREACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreate;
	
	@Column(name = "FECHA_MODIFICACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateModify;
	
}

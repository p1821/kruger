package ec.prueba.kruger.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.prueba.kruger.dto.CreateResponse;
import ec.prueba.kruger.dto.EmployeeDto;
import ec.prueba.kruger.dto.EmployeeRegister;
import ec.prueba.kruger.dto.EmployeeUpdate;
import ec.prueba.kruger.dto.Header;
import ec.prueba.kruger.dto.Response;
import ec.prueba.kruger.dto.ResponseAll;
import ec.prueba.kruger.dto.ResponseEmployee;
import ec.prueba.kruger.entity.Employee;
import ec.prueba.kruger.repository.EmployeeRepository;
import ec.prueba.kruger.util.StatusUtil;

@Service
@Transactional

public class EmployeeServiceImpl implements EmployeeService {
	private static final String METHOD = "Method : {}.";
	private static final String MESSAGEPROCESS = "Proceso realizado en : {} milisegundos.";
	private static final String MESSAGEERROR = "Ha ocurrido un error, intentelo nuevamente.";
	private static final String ACTIVE = "ACTIVO";
	private static final String INACTIVE = "INACTIVO";

	private static final List<String> VACCINETYPE = Arrays.asList("Sputnik", "AstraZeneca", "Pfizer",
			"Jhonson&Jhonson");

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public CreateResponse create(Header header, EmployeeRegister register) {
		
		Date date = new Date();
		CreateResponse request = null;
		
		try {
	

			if(register.getFirstName().chars().anyMatch(Character::isDigit)) {
				request = new CreateResponse(StatusUtil.NOT_CREATED);
	            request.setDescription("El nombre ingresado es inválido. Por favor ingrese un valor válido");
			}else if(register.getLastName().chars().anyMatch(Character::isDigit)) {
				request = new CreateResponse(StatusUtil.NOT_CREATED);
	            request.setDescription("El apellido ingresado es inválido. Por favor ingrese un valor válido");
			}else if (!register.getEmail().contains("@")) { //(!patternEmail.matcher(register.getEmail()).matches())
	        	request = new CreateResponse(StatusUtil.NOT_CREATED);
	            request.setDescription("El email ingresado es inválido. Por favor ingrese un email válido");
			}else if (!register.getIdentification().chars().anyMatch(Character::isDigit) && register.getIdentification().length()<10) {
				request = new CreateResponse(StatusUtil.NOT_CREATED);
	            request.setDescription("El número de identificación ingresado es inválido. Por favor ingrese un valor válido");
			}else {
				
				Employee employee= new Employee();
				employee.setFirstName(register.getFirstName());
				employee.setLastName(register.getLastName());
				employee.setIdentification(register.getIdentification());
				employee.setEmail(register.getEmail());
				String username = register.getFirstName() + register.getLastName();
				employee.setUserName(username);
				String password = RandomStringUtils.randomAlphabetic(10);
				employee.setPassword(password);
				employee.setDateCreate(date);	
				employee.setDateModify(date);
				employee.setUserCreate(header.getUser());
				employee.setUserModify(header.getUser());
				employee.setStatus(ACTIVE);
				
				employeeRepository.save(employee);
				request = new CreateResponse(StatusUtil.CREATED);
				request.setUsername(username);
				request.setPassword(password);
				
			}
		} catch (Exception e) {
			request = new CreateResponse(StatusUtil.INTERNAL_SERVER_ERROR);
			request.setDescription(MESSAGEERROR);
		}	
		return request;
	}
	
	@Override
	public Response updateEmployee(Header header, String identification, EmployeeUpdate update) {
		
		Date date = new Date();
		Response response = null;
		try {
			Employee employee = employeeRepository.findByIdentificationAndStatus(identification, ACTIVE).stream().findFirst().orElse(null);
			if(employee!=null) {
				employee.setDateOfBirth(update.getDateOfBirth());
				employee.setAddress(update.getAddress());
				employee.setTelephone(update.getTelephone());
				employee.setVaccinated(update.getVaccinated());
				if(Boolean.TRUE.equals(update.getVaccinated())){
					if(update.getVaccineType()==null) {
						response = new Response(StatusUtil.NOT_UPDATED);
						response.setDescription("Ingrese el tipo de vacuna.");
					}else if (update.getDateVaccine()==null) {
						response = new Response(StatusUtil.NOT_UPDATED);
						response.setDescription("Ingrese la fecha de vacunación.");
					}else if (update.getNumberDoses()==null) {
						response = new Response(StatusUtil.NOT_UPDATED);
						response.setDescription("Ingrese el número de dosis.");
					}else {
						if(VACCINETYPE.stream().anyMatch(p -> p.equalsIgnoreCase(update.getVaccineType()))) {
							employee.setNumberDoses(update.getNumberDoses());
							employee.setVaccineType(update.getVaccineType());
							employee.setDateVaccine(update.getDateVaccine());
							employee.setDateModify(date);
							employee.setUserModify(header.getUser());

							response = new Response(StatusUtil.UPDATED);
							response.setDescription("Actualización de datos con éxito");
							employeeRepository.saveAndFlush(employee);
						}else {
							response = new Response(StatusUtil.NOT_UPDATED);
							response.setDescription("El tipo de vacuna no existe, ingrese un valor válido.");
						}
					}
				}else {
					response = new Response(StatusUtil.UPDATED);
					employeeRepository.saveAndFlush(employee);
				}
			}else {
				response = new Response(StatusUtil.NOT_UPDATED);
				response.setDescription("El usuario no existe, ingrese un valor válido.");
			}
			
		} catch (Exception e) {
			response = new Response(StatusUtil.INTERNAL_SERVER_ERROR);
			response.setDescription(MESSAGEERROR);
		}
		
		return response;
	}
	@Override
	public Response deletedByIdentification(Header header, String identification) {
		Date date = new Date();
		Response response = null;
		try {
			
			Employee employee = employeeRepository.findByIdentificationAndStatus(identification, ACTIVE).stream().findFirst().orElse(null);
			if (employee != null) {
				employee.setStatus(INACTIVE);
				employee.setDateModify(date);
				employee.setUserModify(header.getUser());
				
				employeeRepository.save(employee);
				response = new Response(StatusUtil.DELETED);
			}else {
				response = new Response(StatusUtil.NOT_DELETED);
				response.setDescription("El usuario no existe, ingrese un valor válido.");
			}
			
			
			
		} catch (Exception e) {
			response = new Response(StatusUtil.INTERNAL_SERVER_ERROR);
			response.setDescription(MESSAGEERROR);
		}
		
		return response;
	}
	@Override
	public ResponseEmployee findByIdentification(Header header, String identification) {
		ResponseEmployee response = null;
		try {
			
			Employee employee = employeeRepository.findByIdentificationAndStatus(identification, ACTIVE).stream().findFirst().orElse(null);
			if (employee != null) {
				EmployeeDto employeeDto = new EmployeeDto();
				employeeDto.setIdentification(employee.getIdentification());
				employeeDto.setFirstName(employee.getFirstName());
				employeeDto.setLastName(employee.getLastName());
				employeeDto.setEmail(employee.getEmail());
				employeeDto.setAddress(employee.getAddress());
				employeeDto.setDateOfBirth(employee.getDateOfBirth());
				employeeDto.setTelephone(employee.getTelephone());
				employeeDto.setVaccinated(employee.getVaccinated());
				employeeDto.setVaccineType(employee.getVaccineType());
				employeeDto.setDateVaccine(employee.getDateVaccine());
				employeeDto.setNumberDoses(employee.getNumberDoses());
				
				response = new ResponseEmployee(StatusUtil.FOUND);
				response.setEmployeeData(employeeDto);
				
			}else {
				response = new ResponseEmployee(StatusUtil.NOT_FOUND);
				response.setDescription("Lo sentimos, El usuario no existe.");
			}
			
			
			
		} catch (Exception e) {
			response = new ResponseEmployee(StatusUtil.INTERNAL_SERVER_ERROR);
			response.setDescription(MESSAGEERROR);
		}
		
		return response;
	}
	@Override
	public ResponseEmployee findByEmail(Header header, String email) {
		ResponseEmployee response = null;
		try {
			
			Employee employee = employeeRepository.findByEmailIgnoreCaseAndStatus(email, ACTIVE).stream().findFirst().orElse(null);
			if (employee != null) {
				EmployeeDto employeeDto = new EmployeeDto();
				employeeDto.setIdentification(employee.getIdentification());
				employeeDto.setFirstName(employee.getFirstName());
				employeeDto.setLastName(employee.getLastName());
				employeeDto.setEmail(employee.getEmail());
				employeeDto.setAddress(employee.getAddress());
				employeeDto.setDateOfBirth(employee.getDateOfBirth());
				employeeDto.setTelephone(employee.getTelephone());
				employeeDto.setVaccinated(employee.getVaccinated());
				employeeDto.setVaccineType(employee.getVaccineType());
				employeeDto.setDateVaccine(employee.getDateVaccine());
				employeeDto.setNumberDoses(employee.getNumberDoses());
								
				response = new ResponseEmployee(StatusUtil.FOUND);
				response.setEmployeeData(employeeDto);
			}else {
				response = new ResponseEmployee(StatusUtil.NOT_FOUND);
				response.setDescription("El usuario no existe.");
			}
			
			
			
		} catch (Exception e) {
			response = new ResponseEmployee(StatusUtil.INTERNAL_SERVER_ERROR);
			response.setDescription(MESSAGEERROR);
		}
		
		return response;
	}
	@Override
	public Response deletedByEmail(Header header, String email) {
		Date date = new Date();
		Response response = null;
		try {
			
			Employee employee = employeeRepository.findByEmailIgnoreCaseAndStatus(email, ACTIVE).stream().findFirst().orElse(null);
			if (employee != null) {
				employee.setStatus(INACTIVE);
				employee.setDateModify(date);
				employee.setUserModify(header.getUser());
				
				employeeRepository.save(employee);
				response = new Response(StatusUtil.DELETED);
			}else {
				response = new Response(StatusUtil.NOT_DELETED);
				response.setDescription("El usuario no existe, ingrese un valor válido.");
			}
			
			
			
		} catch (Exception e) {
			response = new CreateResponse(StatusUtil.INTERNAL_SERVER_ERROR);
			response.setDescription(MESSAGEERROR);
		}
		
		return response;
	}
	@Override
	public ResponseAll find(Header header, Boolean vaccinated, String vaccineType, Date startDate, Date endDate) {
		ResponseAll response = null;
		try {
			
			List<Employee> employeeList = employeeRepository.findByVaccinatedAndVaccineTypeAndDateVaccineBetweenAndStatus(vaccinated, vaccineType, startDate, endDate, ACTIVE);
			if (employeeList.isEmpty()) {
				
				List<EmployeeDto> employeeListDto = new ArrayList<>();
				employeeList.stream().filter(Objects::nonNull).forEach(employee ->{
				
					EmployeeDto employeeDto = new EmployeeDto();
					employeeDto.setIdentification(employee.getIdentification());
					employeeDto.setFirstName(employee.getFirstName());
					employeeDto.setLastName(employee.getLastName());
					employeeDto.setEmail(employee.getEmail());
					employeeDto.setAddress(employee.getAddress());
					employeeDto.setDateOfBirth(employee.getDateOfBirth());
					employeeDto.setTelephone(employee.getTelephone());
					employeeDto.setVaccinated(employee.getVaccinated());
					employeeDto.setVaccineType(employee.getVaccineType());
					employeeDto.setDateVaccine(employee.getDateVaccine());
					employeeDto.setNumberDoses(employee.getNumberDoses());
				});				
				response = new ResponseAll(StatusUtil.FOUND);
				response.setEmployeeData(employeeListDto);
			}else {
				response = new ResponseAll(StatusUtil.NOT_FOUND);
				response.setDescription("El usuario no existe.");
			}			

		} catch (Exception e) {
			response = new ResponseAll(StatusUtil.INTERNAL_SERVER_ERROR);
			response.setDescription(MESSAGEERROR);
		}
		
		return response;
	}

	
}

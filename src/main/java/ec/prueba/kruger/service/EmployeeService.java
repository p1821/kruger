package ec.prueba.kruger.service;

import java.util.Date;

import ec.prueba.kruger.dto.CreateResponse;
import ec.prueba.kruger.dto.EmployeeRegister;
import ec.prueba.kruger.dto.EmployeeUpdate;
import ec.prueba.kruger.dto.Header;
import ec.prueba.kruger.dto.Response;
import ec.prueba.kruger.dto.ResponseAll;
import ec.prueba.kruger.dto.ResponseEmployee;

public interface EmployeeService {

	CreateResponse create(Header header, EmployeeRegister register);
	Response updateEmployee(Header header, String identification, EmployeeUpdate update);
	Response deletedByIdentification(Header header, String identification);
	Response deletedByEmail(Header header, String email);
	ResponseAll find(Header header, Boolean vaccinated, String vaccineType, Date startDate, Date endDate);
	ResponseEmployee findByEmail(Header header, String email);
	ResponseEmployee findByIdentification(Header header, String identification);

}
